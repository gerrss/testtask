﻿using ProductManagement.DomainModel.Abstract;
using ProductManagement.DomainModel.ProductionData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductManagement.DomainModel.UsageData
{
    public class SingleUsage : AbstractUsage
    {
        public int UsageNumber { get; set; }
        public override bool CheckAdd(IDataRepository repository)
        {
            bool productionExsts = repository.Productions.OfType<SingleProduction>().Any(x => x.ProductNumber == UsageNumber) ||
                                   repository.Productions.OfType<RangeProduction>().Any(x => UsageNumber >= x.StartNumber && UsageNumber <= x.EndNumber);
            bool numberInUse = repository.Usages.OfType<SingleUsage>().Any(x => x.UsageNumber == UsageNumber) ||
                               repository.Usages.OfType<RangeUsage>().Any(x => UsageNumber >= x.StartNumber && UsageNumber <= x.EndNumber);

            return productionExsts && !numberInUse;
        }

        public override bool CheckDelete(IDataRepository repository)
        {
            return repository.Usages.OfType<SingleUsage>().Any(x => x.UsageNumber == UsageNumber) ||
                               repository.Usages.OfType<RangeUsage>().Any(x => UsageNumber >= x.StartNumber && UsageNumber <= x.EndNumber);
        }
    }
}
