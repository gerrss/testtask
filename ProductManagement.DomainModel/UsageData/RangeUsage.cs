﻿using ProductManagement.DomainModel.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductManagement.DomainModel.UsageData
{
    public class RangeUsage : AbstractUsage
    {
        public int StartNumber { get; set; }
        public int EndNumber { get; set; }
        public override bool CheckAdd(IDataRepository repository)
        {
            throw new NotImplementedException();
        }

        public override bool CheckDelete(IDataRepository repository)
        {
            throw new NotImplementedException();
        }
    }
}
