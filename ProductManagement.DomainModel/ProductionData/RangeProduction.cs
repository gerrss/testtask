﻿using ProductManagement.DomainModel.Abstract;
using ProductManagement.DomainModel.UsageData;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductManagement.DomainModel.ProductionData
{
    public class RangeProduction : AbstractProduction
    {
        [DisplayName("Начальный номпер диапазона продукции")]
        [Range(1, 9999999999, ErrorMessage = "Номер должен быть в диапазоне от 1 до 9999999999 (десятизначное целое)")]
        [Required(ErrorMessage = "Номер не может быть пустым")]
        public int StartNumber { get; set; }
       
        [DisplayName("Конечный номер диапазона продукции")]
        [Range(1, 9999999999, ErrorMessage = "Номер должен быть в диапазоне от 1 до 9999999999 (десятизначное целое)")]
        [Required(ErrorMessage = "Номер не может быть пустым")]
        public int EndNumber { get; set; }

        public override bool CheckAdd(IDataRepository repository)
        {
            bool singles = !repository.Productions.OfType<SingleProduction>().Any(x => x.ProductNumber >= StartNumber && x.ProductNumber <= EndNumber);
            bool rangesStartIntersect = !repository.Productions.OfType<RangeProduction>().Any(x => StartNumber >= x.StartNumber && StartNumber <= x.EndNumber);
            bool rangesEndIntersect = !repository.Productions.OfType<RangeProduction>().Any(x => EndNumber >= x.StartNumber && EndNumber <= x.EndNumber);
            bool rangesExistingInNew = !repository.Productions.OfType<RangeProduction>().Any(x => StartNumber <= x.StartNumber && EndNumber >= x.EndNumber);

            var existingInNewIds = repository.Productions.OfType<RangeProduction>().Where(x => StartNumber <= x.StartNumber && EndNumber >= x.EndNumber).Select(x => x.ProductionId);

            if (ProductionId != 0 && existingInNewIds.Count() == 1 && existingInNewIds.First() == ProductionId)
            {
                rangesExistingInNew = true;
                rangesEndIntersect = true;
                rangesStartIntersect = true;
            }

            bool forNew = singles && rangesStartIntersect && rangesEndIntersect && rangesExistingInNew;
            if (ProductionId == 0)
                return forNew;

            RangeProduction oldValue = repository.Productions.OfType<RangeProduction>().FirstOrDefault(x => x.ProductionId == ProductionId);

            if (oldValue == null)
                return forNew;

            bool hasSingleUsage = repository.Usages.OfType<SingleUsage>().Any(x => (x.UsageNumber >= oldValue.StartNumber && x.UsageNumber < StartNumber) || (x.UsageNumber > EndNumber && x.UsageNumber <= oldValue.EndNumber));
            bool hasRangeUsage = repository.Usages
                .OfType<RangeUsage>()
                .Any(x => (x.StartNumber >= oldValue.StartNumber && x.StartNumber < StartNumber) ||
                          (x.EndNumber >= oldValue.StartNumber && x.EndNumber < StartNumber) ||
                          (x.StartNumber > EndNumber && x.StartNumber <= oldValue.EndNumber) ||
                          (x.EndNumber > EndNumber && x.EndNumber <= oldValue.EndNumber)
                );

            return forNew && !hasSingleUsage && !hasRangeUsage;
        }

        public override bool CheckDelete(IDataRepository repository)
        {
            bool hasSingleUsage = repository.Usages.OfType<SingleUsage>().Any(x => (x.UsageNumber >= StartNumber && x.UsageNumber <= EndNumber));
            bool hasRangeUsage = repository.Usages
               .OfType<RangeUsage>()
               .Any(x => (x.EndNumber >= StartNumber && x.EndNumber <= EndNumber) ||
                         (x.StartNumber >= StartNumber && x.StartNumber <= EndNumber)
               );

            return !hasRangeUsage && !hasSingleUsage;
        }

        public override string ToString()
        {
            return string.Format("{0} - {1}", StartNumber, EndNumber);
        }
    }
}
