﻿using ProductManagement.DomainModel.Abstract;
using ProductManagement.DomainModel.UsageData;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductManagement.DomainModel.ProductionData
{
    public class SingleProduction : AbstractProduction
    {
        [DisplayName("Номер продукции")]
        [Range(1, 9999999999, ErrorMessage="Номер должен быть в диапазоне от 1 до 9999999999 (десятизначное целое)")]
        [Required(ErrorMessage="Номер не может быть пустым")]
        public int ProductNumber { get; set; }

        public override bool CheckAdd(IDataRepository repository)
        {
            bool singles = !repository.Productions.OfType<SingleProduction>().Any(x => x.ProductNumber == ProductNumber);
            bool ranges = !repository.Productions.OfType<RangeProduction>().Any(x => x.StartNumber <= ProductNumber && x.EndNumber >= ProductNumber);

            if (ProductionId == 0)
            {
                return singles && ranges;
            }
            else
            {
                SingleProduction old = repository.Productions.OfType<SingleProduction>().FirstOrDefault(x => x.ProductionId == ProductionId);

                if (old == null)
                    return singles && ranges;
                if (ProductionId == old.ProductionId && ProductNumber == old.ProductNumber)
                    return true;

                bool newExists = repository.Productions.OfType<SingleProduction>().Any(x => x.ProductNumber == ProductNumber);
                bool oldUsed = repository.Usages.OfType<SingleUsage>().Any(x => x.UsageNumber == old.ProductNumber);
                bool oldUsedRange = repository.Usages.OfType<RangeUsage>().Any(x => x.StartNumber <= old.ProductNumber && x.EndNumber >= old.ProductNumber);

                return singles && ranges && !newExists && !oldUsed && !oldUsedRange;
            }
        }

        public override bool CheckDelete(IDataRepository repository)
        {
            return !(repository.Usages.OfType<SingleUsage>().Any(x => x.UsageNumber == ProductNumber) ||
                               repository.Usages.OfType<RangeUsage>().Any(x => ProductNumber >= x.StartNumber && ProductNumber <= x.EndNumber));
        }

        public override string ToString()
        {
            return ProductNumber.ToString();
        }
    }
}
