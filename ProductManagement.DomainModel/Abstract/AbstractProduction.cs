﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace ProductManagement.DomainModel.Abstract
{
    public abstract class AbstractProduction
    {
        [DisplayName("Дата производства")]
        [Required(ErrorMessage="Дата производства не может быть пустой")]
        public DateTime ProductionDate { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int ProductionId { get; set; }

        public abstract bool CheckAdd(IDataRepository repository);

        public abstract bool CheckDelete(IDataRepository repository);
    }
}
