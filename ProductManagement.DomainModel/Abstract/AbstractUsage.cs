﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductManagement.DomainModel.Abstract
{
    public abstract class AbstractUsage
    {
        public int UsageId { get; set; }
        public DateTime UsageDate { get; set; }

        public abstract bool CheckAdd(IDataRepository repository);

        public abstract bool CheckDelete(IDataRepository repository);
    }
}
