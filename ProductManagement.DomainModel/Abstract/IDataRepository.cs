﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductManagement.DomainModel.Abstract
{
    public interface IDataRepository
    {
        IQueryable<AbstractProduction> Productions { get; }
        IQueryable<AbstractUsage> Usages { get; }

        void SaveProduction(AbstractProduction production);
        void DeleteProduction(int productionId);

        void SaveUsage(AbstractUsage usage);
        void DeleteUsage(int usageId);
    }
}
