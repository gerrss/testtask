﻿using ProductManagement.DomainModel.Abstract;
using ProductManagement.DomainModel.ProductionData;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductManagement.DomainModel.DataRepository
{
    public class SqlDBDataRepository : IDataRepository
    {
        private ProductionDBEntities dbContext;

        public SqlDBDataRepository()
        {
            dbContext = new ProductionDBEntities();
        }

        public IQueryable<AbstractProduction> Productions
        {
            get 
            {
                var singleProductions = dbContext.Production.Where(x => x.EndNumber == null).Select(x => new SingleProduction() { ProductionId = x.id, ProductionDate = x.ProductionDate, ProductNumber = x.Number }).ToList<AbstractProduction>();
                var rangeProductions = dbContext.Production.Where(x => x.EndNumber != null).Select(x => new RangeProduction() { ProductionId = x.id, ProductionDate = x.ProductionDate, StartNumber = x.Number, EndNumber = x.EndNumber.Value }).ToList<AbstractProduction>();

                return singleProductions.Concat(rangeProductions).AsQueryable();
            }
        }

        public IQueryable<AbstractUsage> Usages
        {
            get { return new List<AbstractUsage>().AsQueryable(); }
        }

        public void SaveProduction(AbstractProduction production)
        {
            if (!production.CheckAdd(this))
                throw new InvalidOperationException("Номер или диапазон номеров не могут быть сохранены в БД");

            Production dbProduction;

            if (production.ProductionId == 0)
            {
                 dbProduction = new Production() 
                    { 
                        ProductionDate = production.ProductionDate, 
                        Number = production is SingleProduction ? ((SingleProduction)production).ProductNumber : ((RangeProduction)production).StartNumber,
                        EndNumber = production is SingleProduction ? (int?)null : ((RangeProduction)production).EndNumber
                    };
                dbContext.Production.Add(dbProduction);
            }
            else
            {
                dbProduction = dbContext.Production.FirstOrDefault(x => x.id == production.ProductionId);
                if (dbProduction == null)
                {
                    dbProduction = new Production()
                    {
                        ProductionDate = production.ProductionDate,
                        Number = production is SingleProduction ? ((SingleProduction)production).ProductNumber : ((RangeProduction)production).StartNumber,
                        EndNumber = production is SingleProduction ? (int?)null : ((RangeProduction)production).EndNumber
                    };
                    dbContext.Production.Add(dbProduction);
                }
                else
                {
                    dbProduction.ProductionDate = production.ProductionDate;
                    dbProduction.Number = production is SingleProduction ? ((SingleProduction)production).ProductNumber : ((RangeProduction)production).StartNumber;
                    dbProduction.EndNumber = production is SingleProduction ? (int?)null : ((RangeProduction)production).EndNumber;
                }
            }

            dbContext.SaveChanges();
        }

        public void DeleteProduction(int productionId)
        {
            var production = Productions.FirstOrDefault(x => x.ProductionId == productionId);

            if (production == null)
                throw new ObjectNotFoundException(string.Format("Производство с кодом {0} не существует.Удаление невозможно", productionId));

            if (!production.CheckDelete(this))
                throw new InvalidOperationException(string.Format("Производство с кодом {0} не может быть удалено - полностью или частично используется.", productionId));

            var dbProduction = dbContext.Production.FirstOrDefault(x => x.id == productionId);

            if (dbProduction == null)
                throw new ObjectNotFoundException(string.Format("Производство с кодом {0} не существует.Удаление невозможно", productionId));

            dbContext.Production.Remove(dbProduction);
            dbContext.SaveChanges();
        }

        public void SaveUsage(AbstractUsage usage)
        {
            throw new NotImplementedException();
        }

        public void DeleteUsage(int usageId)
        {
            throw new NotImplementedException();
        }
    }
}
