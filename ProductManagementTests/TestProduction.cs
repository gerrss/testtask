﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using ProductManagement.DomainModel.Abstract;
using ProductManagement.DomainModel.ProductionData;
using System.Collections.Generic;
using System.Linq;
using ProductManagement.DomainModel.UsageData;

namespace ProductManagementTests
{
    [TestClass]
    public class TestProduction
    {
        private IDataRepository CreateRepository()
        {
            Mock<IDataRepository> mock = new Mock<IDataRepository>();
            mock.Setup(m => m.Productions).Returns(new AbstractProduction[] {
                new SingleProduction { ProductionId = 1, ProductionDate = DateTime.Today, ProductNumber = 1},
                new SingleProduction { ProductionId = 2, ProductionDate = DateTime.Today, ProductNumber = 2},
                new SingleProduction { ProductionId = 3, ProductionDate = DateTime.Today, ProductNumber = 4},
                new RangeProduction  { ProductionId = 4, ProductionDate = DateTime.Today, StartNumber = 6, EndNumber = 10},
                new RangeProduction  { ProductionId = 5, ProductionDate = DateTime.Today, StartNumber = 100, EndNumber = 200},
                new SingleProduction { ProductionId = 6, ProductionDate = DateTime.Today, ProductNumber = 501},
                new SingleProduction { ProductionId = 7, ProductionDate = DateTime.Today, ProductNumber = 502},
                new SingleProduction { ProductionId = 8, ProductionDate = DateTime.Today, ProductNumber = 503},
                new SingleProduction { ProductionId = 9, ProductionDate = DateTime.Today, ProductNumber = 504},
                new RangeProduction  { ProductionId = 10, StartNumber = 510, EndNumber = 520 }
            }.AsQueryable());

            mock.Setup(m => m.Usages).Returns(new AbstractUsage[] {
                new SingleUsage { UsageId = 1, UsageDate = DateTime.Today, UsageNumber = 1 },
                new SingleUsage { UsageId = 2, UsageDate = DateTime.Today, UsageNumber = 4 },
                new RangeUsage  { UsageId = 3, UsageDate = DateTime.Today, StartNumber = 7, EndNumber = 9 },
                new RangeUsage  { UsageId = 4, UsageDate = DateTime.Today, StartNumber = 501, EndNumber = 504 },
                new SingleUsage { UsageId = 5, UsageNumber = 515}
                }.AsQueryable());

            return mock.Object;
        }

        #region Tests for Rules to add Production - single and range
        [TestMethod]
        public void Test_SingleProduction_CheckAdd_Correct()
        {
            IDataRepository repository = CreateRepository();

            SingleProduction production = new SingleProduction() { ProductionDate = DateTime.Today, ProductNumber = 3 };
            bool result = production.CheckAdd(repository);

            Assert.AreEqual(true, result);
        }

        [TestMethod]
        public void Test_SingleProduction_CheckAdd_Incorrect_Single()
        {
            IDataRepository repository = CreateRepository();

            SingleProduction production = new SingleProduction() { ProductionDate = DateTime.Today, ProductNumber = 2 };
            bool result = production.CheckAdd(repository);

            Assert.AreEqual(false, result);
        }

        [TestMethod]
        public void Test_SingleProduction_CheckAdd_Incorrect_Range()
        {
            IDataRepository repository = CreateRepository();

            SingleProduction production = new SingleProduction() { ProductionDate = DateTime.Today, ProductNumber = 8 };
            bool result = production.CheckAdd(repository);

            Assert.AreEqual(false, result);
        }

        [TestMethod]
        public void Test_RangeProduction_CheckAdd_Correct()
        {
            IDataRepository repository = CreateRepository();

            RangeProduction production = new RangeProduction() { ProductionDate = DateTime.Today, StartNumber = 11, EndNumber = 20 };
            bool result = production.CheckAdd(repository);

            Assert.AreEqual(true, result);
        }
        
        [TestMethod]
        public void Test_RangeProduction_CheckAdd_SingleInCorrect()
        {
            IDataRepository repository = CreateRepository();

            RangeProduction production = new RangeProduction() { ProductionDate = DateTime.Today, StartNumber = 3, EndNumber = 5 };
            bool result = production.CheckAdd(repository);

            Assert.AreEqual(false, result);
        }

        [TestMethod]
        public void Test_RangeProduction_CheckAdd_RangeInCorrect_Start()
        {
            IDataRepository repository = CreateRepository();

            RangeProduction production = new RangeProduction() { ProductionDate = DateTime.Today, StartNumber = 8, EndNumber = 15 };
            bool result = production.CheckAdd(repository);

            Assert.AreEqual(false, result);
        }
        
        [TestMethod]
        public void Test_RangeProduction_CheckAdd_RangeInCorrect_End()
        {
            IDataRepository repository = CreateRepository();

            RangeProduction production = new RangeProduction() { ProductionDate = DateTime.Today, StartNumber = 5, EndNumber = 7 };
            bool result = production.CheckAdd(repository);

            Assert.AreEqual(false, result);
        }

        [TestMethod]
        public void Test_RangeProduction_CheckAdd_RangeInCorrect_NewInExisting()
        {
            IDataRepository repository = CreateRepository();

            RangeProduction production = new RangeProduction() { ProductionDate = DateTime.Today, StartNumber = 7, EndNumber = 9 };
            bool result = production.CheckAdd(repository);

            Assert.AreEqual(false, result);
        }

        [TestMethod]
        public void Test_RangeProduction_CheckAdd_RangeInCorrect_ExistingInNew()
        {
            IDataRepository repository = CreateRepository();

            RangeProduction production = new RangeProduction() { ProductionDate = DateTime.Today, StartNumber = 5, EndNumber = 11 };
            bool result = production.CheckAdd(repository);

            Assert.AreEqual(false, result);
        }
        #endregion

        #region Tests for edit Production - single and range
        [TestMethod]
        public void Test_SingleProduction_CheckAddEdit_Correct()
        {
            IDataRepository repository = CreateRepository();
            SingleProduction production = new SingleProduction() { ProductionId = 2, ProductNumber = 3 };

            bool result = production.CheckAdd(repository);
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void Test_SingleProduction_CheckAddEdit_Incorrect_SingleIsUsed()
        {
            IDataRepository repository = CreateRepository();
            SingleProduction production = new SingleProduction() { ProductionId = 3, ProductNumber = 20 };

            bool result = production.CheckAdd(repository);
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void Test_SingleProduction_CheckAddEdit_Incorrect_RangeIsUsed()
        {
            IDataRepository repository = CreateRepository();
            SingleProduction production = new SingleProduction() { ProductionId = 8, ProductNumber = 505 };

            bool result = production.CheckAdd(repository);

            Assert.IsFalse(result);
        }


        [TestMethod]
        public void Test_RangeProduction_CheckAddEdit_Correct()
        {
            IDataRepository repository = CreateRepository();
            RangeProduction production = new RangeProduction() { ProductionId = 4, StartNumber = 5, EndNumber = 11 };

            bool result = production.CheckAdd(repository);

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void Test_RangeProduction_CheckAddEdit_Incorrect_SingleIsUsed()
        {
            IDataRepository repository = CreateRepository();
            RangeProduction productionEndCut = new RangeProduction() { ProductionId = 10, StartNumber = 510, EndNumber = 514 };
            RangeProduction productionStartCut = new RangeProduction() { ProductionId = 10, StartNumber = 516, EndNumber = 525 };

            bool resultEndCut = productionEndCut.CheckAdd(repository);
            bool resultStartCut = productionStartCut.CheckAdd(repository);

            Assert.IsFalse(resultEndCut);
            Assert.IsFalse(resultStartCut);
        }

        [TestMethod]
        public void Test_RangeProduction_CheckAddEdit_Incorrect_RegionIsUsed()
        {
            IDataRepository repository = CreateRepository();
            RangeProduction productionEndCut = new RangeProduction() { ProductionId = 4, StartNumber = 6, EndNumber = 8 };
            RangeProduction productionStartCut = new RangeProduction() { ProductionId = 4, StartNumber = 8, EndNumber = 10 };

            bool resultEndCut = productionEndCut.CheckAdd(repository);
            bool resultStartCut = productionStartCut.CheckAdd(repository);

            Assert.IsFalse(resultEndCut);
            Assert.IsFalse(resultStartCut);
        }
        #endregion

        [TestMethod]
        public void Test_SingleProduction_CanDelete_Correct()
        {
            IDataRepository repository = CreateRepository();
            SingleProduction prod = new SingleProduction() { ProductionId = 2, ProductNumber = 2 };

            var result = prod.CheckDelete(repository);

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void Test_SingleProduction_CheckDelete_Incorrect()
        {
            IDataRepository repository = CreateRepository();
            SingleProduction prod = new SingleProduction() { ProductionId = 1, ProductNumber = 1 };
            SingleProduction prod1 = new SingleProduction() { ProductionId = 7, ProductNumber = 502 };

            var result = prod.CheckDelete(repository);
            var result1 = prod1.CheckDelete(repository);

            Assert.IsFalse(result);
            Assert.IsFalse(result1);
       }
       
        [TestMethod]
        public void Test_RangeProduction_CanDelete_Correct()
        {
            IDataRepository repository = CreateRepository();
            RangeProduction prod = new RangeProduction() { ProductionId = 5, StartNumber =  100, EndNumber = 200 };

            var result = prod.CheckDelete(repository);

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void Test_RangeProduction_CheckDelete_Incorrect()
        {
            IDataRepository repository = CreateRepository();
            RangeProduction prod = new RangeProduction() { ProductionId = 10, StartNumber = 510, EndNumber = 520 };
            RangeProduction prod1 = new RangeProduction() { ProductionId = 4, StartNumber = 6, EndNumber = 10 };

            var result = prod.CheckDelete(repository);
            var result1 = prod1.CheckDelete(repository);

            Assert.IsFalse(result);
            Assert.IsFalse(result1);
        }
    }
}
