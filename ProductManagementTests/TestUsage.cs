﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProductManagement.DomainModel.Abstract;
using System.Collections.Generic;
using System.Linq;
using Moq;
using ProductManagement.DomainModel.ProductionData;
using ProductManagement.DomainModel.UsageData;

namespace ProductManagementTests
{
    [TestClass]
    public class TestUsage
    {
        private IDataRepository CreateRepository()
        {
            Mock<IDataRepository> mock = new Mock<IDataRepository>();
            mock.Setup(m => m.Productions).Returns(new AbstractProduction[] {
                new SingleProduction { ProductionId = 1, ProductionDate = DateTime.Today, ProductNumber = 1},
                new SingleProduction { ProductionId = 2, ProductionDate = DateTime.Today, ProductNumber = 2},
                new SingleProduction { ProductionId = 3, ProductionDate = DateTime.Today, ProductNumber = 4},
                new RangeProduction  { ProductionId = 4, ProductionDate = DateTime.Today, StartNumber = 6, EndNumber = 10},
                new RangeProduction  { ProductionId = 5, ProductionDate = DateTime.Today, StartNumber = 100, EndNumber = 200},
                new SingleProduction { ProductionId = 6, ProductionDate = DateTime.Today, ProductNumber = 501},
                new SingleProduction { ProductionId = 7, ProductionDate = DateTime.Today, ProductNumber = 502},
                new SingleProduction { ProductionId = 8, ProductionDate = DateTime.Today, ProductNumber = 503},
                new SingleProduction { ProductionId = 9, ProductionDate = DateTime.Today, ProductNumber = 504},
                new RangeProduction  { ProductionId = 10, StartNumber = 510, EndNumber = 520 }
            }.AsQueryable());

            mock.Setup(m => m.Usages).Returns(new AbstractUsage[] {
                new SingleUsage { UsageId = 1, UsageDate = DateTime.Today, UsageNumber = 1 },
                new SingleUsage { UsageId = 2, UsageDate = DateTime.Today, UsageNumber = 4 },
                new RangeUsage  { UsageId = 3, UsageDate = DateTime.Today, StartNumber = 7, EndNumber = 9 },
                new RangeUsage  { UsageId = 4, UsageDate = DateTime.Today, StartNumber = 501, EndNumber = 504 },
                new SingleUsage { UsageId = 5, UsageNumber = 515}
                }.AsQueryable());

            return mock.Object;
        }

        [TestMethod]
        public void Test_SingleUsage_CheckAddOrEdit_Correct()
        {
            IDataRepository repository = CreateRepository();
            SingleUsage newUsage = new SingleUsage() { UsageId = 6, UsageNumber = 2 };
            SingleUsage newUsageInRange = new SingleUsage() { UsageId = 7, UsageNumber = 120 };

            SingleUsage exitingUsage = new SingleUsage() { UsageId = 2, UsageNumber = 2 };
            SingleUsage existingUsageInRange = new SingleUsage() { UsageId = 5, UsageNumber = 517 };

            bool result = newUsage.CheckAdd(repository);
            bool resultRange = newUsageInRange.CheckAdd(repository);
            bool resultExisting = exitingUsage.CheckAdd(repository);
            bool resultExistingInRange = existingUsageInRange.CheckAdd(repository);

            Assert.IsTrue(result);
            Assert.IsTrue(resultRange);
            Assert.IsTrue(resultExisting);
            Assert.IsTrue(resultExistingInRange);
        }
    }
}
