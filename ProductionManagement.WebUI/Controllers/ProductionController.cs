﻿using ProductionManagement.WebUI.Infrastructure;
using ProductManagement.DomainModel.Abstract;
using ProductManagement.DomainModel.ProductionData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProductionManagement.WebUI.Controllers
{
    public class ProductionController : Controller
    {
        private IDataRepository repository;

        public ProductionController(IDataRepository repo)
        {
            repository = repo;
        }

        public ActionResult AddNewSingle()
        {
            SingleProduction production = new SingleProduction();
            return View(production);
        }

        [HttpPost]
        [OutOfRangeException]
        public ActionResult AddNewSingle(SingleProduction production)
        {
            if (ModelState.IsValid)
            {
                repository.SaveProduction(production);
                return RedirectToAction("Index", "Home");
            }
            return View(production);
        }

        public ActionResult AddNewRange()
        {
            RangeProduction production = new RangeProduction();
            return View(production);
        }

        [HttpPost]
        [OutOfRangeException]
        public ActionResult AddNewRange(RangeProduction production)
        {
            if (ModelState.IsValid)
            {
                repository.SaveProduction(production);
                return RedirectToAction("Index", "Home");
            }
            return View(production);
        }

        public ActionResult Edit(int? productionId)
        {
            if (productionId.HasValue)
            {
                var production = repository.Productions.FirstOrDefault(x => x.ProductionId == productionId.Value);
                if (production == null)
                    return View("Error", "Не найден объект для редактирования");

                if (production is SingleProduction)
                    return View("AddNewSingle", production);
                else return View("AddNewRange", production);
            }
            else return View("Error", "Не найден объект для редактирования");
        }

        [HttpPost]
        [DeleteExcepton]
        public ActionResult Delete(int productionId)
        {
            repository.DeleteProduction(productionId);
            return RedirectToAction("Index", "Home");
        }
    }
}
