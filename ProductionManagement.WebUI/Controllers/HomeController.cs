﻿using ProductManagement.DomainModel.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProductionManagement.WebUI.Controllers
{
    public class HomeController : Controller
    {
        private IDataRepository repository;
        public HomeController(IDataRepository repo)
        {
            repository = repo;
        }
        public ViewResult Index()
        {
            return View(repository.Productions);
        }

    }
}
