﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProductionManagement.WebUI.Infrastructure
{
    public class DeleteExceptonAttribute : FilterAttribute, IExceptionFilter
    {
        public void OnException(ExceptionContext filterContext)
        {
            if (!filterContext.ExceptionHandled && (filterContext.Exception is ObjectNotFoundException || filterContext.Exception is InvalidOperationException))
            {
                filterContext.Result = new ViewResult()
                {
                    ViewName = "Error",
                    ViewData = new ViewDataDictionary(filterContext.Exception.Message)
                };

                filterContext.ExceptionHandled = true;
            }
        }
    }
}