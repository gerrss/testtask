﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProductManagement.DomainModel.Abstract;
using Moq;
using ProductManagement.DomainModel.ProductionData;
using ProductManagement.DomainModel.UsageData;
using System.Linq;
using ProductionManagement.WebUI.Controllers;

namespace ProductionManagement.WebUI.Tests
{
    [TestClass]
    public class HomeControllerTest
    {
        
        [TestMethod]
        public void TestIndex()
        {
             Mock<IDataRepository> mock = new Mock<IDataRepository>();
            mock.Setup(m => m.Productions).Returns(new AbstractProduction[] {
                new SingleProduction { ProductionId = 1, ProductionDate = DateTime.Today, ProductNumber = 1},
                new SingleProduction { ProductionId = 2, ProductionDate = DateTime.Today, ProductNumber = 2},
                new SingleProduction { ProductionId = 3, ProductionDate = DateTime.Today, ProductNumber = 4},
                new RangeProduction  { ProductionId = 4, ProductionDate = DateTime.Today, StartNumber = 6, EndNumber = 10},
                new RangeProduction  { ProductionId = 5, ProductionDate = DateTime.Today, StartNumber = 100, EndNumber = 200},
                new SingleProduction { ProductionId = 6, ProductionDate = DateTime.Today, ProductNumber = 501},
                new SingleProduction { ProductionId = 7, ProductionDate = DateTime.Today, ProductNumber = 502},
                new SingleProduction { ProductionId = 8, ProductionDate = DateTime.Today, ProductNumber = 503},
                new SingleProduction { ProductionId = 9, ProductionDate = DateTime.Today, ProductNumber = 504},
                new RangeProduction  { ProductionId = 10, StartNumber = 510, EndNumber = 520 }
            }.AsQueryable());

            mock.Setup(m => m.Usages).Returns(new AbstractUsage[] {
                new SingleUsage { UsageId = 1, UsageDate = DateTime.Today, UsageNumber = 1 },
                new SingleUsage { UsageId = 2, UsageDate = DateTime.Today, UsageNumber = 4 },
                new RangeUsage  { UsageId = 3, UsageDate = DateTime.Today, StartNumber = 7, EndNumber = 9 },
                new RangeUsage  { UsageId = 4, UsageDate = DateTime.Today, StartNumber = 501, EndNumber = 504 },
                new SingleUsage { UsageId = 5, UsageNumber = 515}
                }.AsQueryable());

            HomeController controller = new HomeController(mock.Object);

            var result = controller.Index();
            var data = ((IQueryable<AbstractProduction>)result.Model).ToArray();
            Assert.IsTrue(data.Length == 10);
        }
    }
}
