﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using Moq;
using ProductManagement.DomainModel.Abstract;
using ProductManagement.DomainModel.ProductionData;
using ProductManagement.DomainModel.UsageData;
using ProductionManagement.WebUI.Controllers;
using System.Web.Mvc;

namespace ProductionManagement.WebUI.Tests
{
    [TestClass]
    public class ProductionControllerTest
    {

        private Mock<IDataRepository> CreateMock()
        {
            Mock<IDataRepository> mock = new Mock<IDataRepository>();
            mock.Setup(m => m.Productions).Returns(new AbstractProduction[] {
                new SingleProduction { ProductionId = 1, ProductionDate = DateTime.Today, ProductNumber = 1},
                new SingleProduction { ProductionId = 2, ProductionDate = DateTime.Today, ProductNumber = 2},
                new SingleProduction { ProductionId = 3, ProductionDate = DateTime.Today, ProductNumber = 4},
                new RangeProduction  { ProductionId = 4, ProductionDate = DateTime.Today, StartNumber = 6, EndNumber = 10},
                new RangeProduction  { ProductionId = 5, ProductionDate = DateTime.Today, StartNumber = 100, EndNumber = 200},
                new SingleProduction { ProductionId = 6, ProductionDate = DateTime.Today, ProductNumber = 501},
                new SingleProduction { ProductionId = 7, ProductionDate = DateTime.Today, ProductNumber = 502},
                new SingleProduction { ProductionId = 8, ProductionDate = DateTime.Today, ProductNumber = 503},
                new SingleProduction { ProductionId = 9, ProductionDate = DateTime.Today, ProductNumber = 504},
                new RangeProduction  { ProductionId = 10, StartNumber = 510, EndNumber = 520 }
            }.AsQueryable());

            mock.Setup(m => m.Usages).Returns(new AbstractUsage[] {
                new SingleUsage { UsageId = 1, UsageDate = DateTime.Today, UsageNumber = 1 },
                new SingleUsage { UsageId = 2, UsageDate = DateTime.Today, UsageNumber = 4 },
                new RangeUsage  { UsageId = 3, UsageDate = DateTime.Today, StartNumber = 7, EndNumber = 9 },
                new RangeUsage  { UsageId = 4, UsageDate = DateTime.Today, StartNumber = 501, EndNumber = 504 },
                new SingleUsage { UsageId = 5, UsageNumber = 515}
                }.AsQueryable());

            return mock;
        }

        [TestMethod]
        public void TestAddNew_Correct()
        {
            Mock<IDataRepository> mock = CreateMock();
            ProductionController controller = new ProductionController(mock.Object);
            SingleProduction newSingle = new SingleProduction() { ProductNumber = 1000 };
            var result = controller.AddNewSingle(newSingle);

            mock.Verify(m => m.SaveProduction(newSingle));
            Assert.IsNotInstanceOfType(result, typeof(ViewResult));
        }

        [TestMethod]
        public void TestAddNewRange_Correct()
        {
            Mock<IDataRepository> mock = CreateMock();
            ProductionController controller = new ProductionController(mock.Object);
            RangeProduction production = new RangeProduction() { ProductionDate = DateTime.Today, StartNumber = 1000, EndNumber = 2000 };

            var result = controller.AddNewRange(production);

            mock.Verify(m => m.SaveProduction(production));
            Assert.IsNotInstanceOfType(result, typeof(ViewResult));
        }

        [TestMethod]
        public void TestEditSingle_Correct()
        {
            Mock<IDataRepository> mock = CreateMock();
            ProductionController controller = new ProductionController(mock.Object);

            var resultSingle = controller.Edit(1);

            Assert.AreEqual(1, ((SingleProduction)((ViewResult)resultSingle).Model).ProductNumber);
        }

        [TestMethod]
        public void TestEditMultiple_Correct()
        {
            Mock<IDataRepository> mock = CreateMock();
            ProductionController controller = new ProductionController(mock.Object);

            var resultMultiple = controller.Edit(4);

            Assert.AreEqual(6, ((RangeProduction)((ViewResult)resultMultiple).Model).StartNumber);
            Assert.AreEqual(10, ((RangeProduction)((ViewResult)resultMultiple).Model).EndNumber);
        }
        
        [TestMethod]
        public void TestEdit_Incorrect()
        {
            Mock<IDataRepository> mock = CreateMock();
            ProductionController controller = new ProductionController(mock.Object);

            var result = controller.Edit(5000);

            Assert.AreEqual("Error", ((ViewResult)result).ViewName);
        }

        [TestMethod]
        public void TestDelete()
        {
            Mock<IDataRepository> mock = CreateMock();

            ProductionController controller = new ProductionController(mock.Object);
            var result = controller.Delete(2);

            mock.Verify(m => m.DeleteProduction(2));
            Assert.IsNotInstanceOfType(result, typeof(ViewResult));
        }
    }
}
